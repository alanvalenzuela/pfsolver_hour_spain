# coding: utf-8
import pandas as pd
import json

with open('data/powerflowresults.json') as f:
    data = json.load(f)

output_supply = pd.DataFrame(data["supply_points"])

f = open('max_v.csv', 'w')
# NODES = ['CT_AURELIO_LV', 'CT_HERMENEGILDO TORO VEGA_LV', 'CT_TELEFONICA FUENSANTA_LV']
# f.write("VMAX=" + str(output_supply[output_supply.name.isin(NODES)].vm_pu.max()))
f.write("VMAX=" + str(output_supply[~output_supply.name.str.contains("LV")]['vm_pu'].max()))
f.close()
