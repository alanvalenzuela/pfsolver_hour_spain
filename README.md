# Iterative code

This codes runs the powerflow solver for each hour, increasing the demand to achieve a similar voltage to a previously defined case. The algorithm uses a bash script for general purposes and python scripts for modifying or reading files, using pandas.

## Files used

### run.sh
Main script for running. Does the following:

  - For each HOUR:
    - Initialize ALPHA, VMAX high
    - Read voltage limit VLIM from HOUR (get_voltage_hour.py)
    - While VMAX > VLIM do:
      - Update json for running powerflow for current HOUR with current ALPHA (update_json.py)
      - Solve powerflow (curl request)
      - Read current highest voltage as VMAX
      - Increase ALPHA
    - When ending, write current data (HOUR, ALPHA, VMAX, PDIFF) into results.csv and go to next hour

### get_voltage_hour.py

Writes current limit for voltage in "v_lim.csv". Uses as argument current HOUR.

### update_json.py

Loads full_data_normal.json and updates the current hour. Uses as arguments ALPHA (demand percentage increase) and HOUR. Depending on the folder, will increase the active demand in the defined meters. Demand is multiplied by (1 + ALPHA/100). Current power difference is written in power_diff.csv file.


### export_csv_map.py

Loads current results and gets maximum MV voltage value written into max_v.csv file.

### full_data_normal.json

Current network data being modified.
