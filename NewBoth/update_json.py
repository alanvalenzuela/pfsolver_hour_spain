# coding: utf-8
import json
import pandas as pd
import sys

with open('full_data_normal.json') as f:
    data = json.load(f)


def dataframe_to_json(df):
    values = []
    for i in df.index:
        values.append(eval(df.loc[i].to_json().replace(",\"is_pf\":null", "").replace("true", "\"true\"")))
    return values

# Inputs values
alpha = float(sys.argv[1]) / 100
hour = str(sys.argv[2])

# Run 3 pm only
time = f"2020-03-12 {hour}:00:00+00:00"
data['configuration']['fromDate'] = time
data['configuration']['toDate'] = time
loads_df = pd.DataFrame(data['loads'])
loads_df.time = pd.to_datetime(loads_df.time)

# For paraiso, espafron, beljado, peñuelas 2, expand loads
meters = ["ZIV0004480052", "933", "505", "ZIV0004397349", "ZIV0004397350"] + [
    "ZIV0004324976", "ZIV0004449798", "ZIV0004332302", "ZIV0004397393",
    "ZIV0004394468", "ZIV0004394465", "ZIV0004449800", "207", "208"]
idx = loads_df.loc[(loads_df.meter.isin(meters)) & (loads_df.time == time)].index

# Save power difference
power_diff = round(sum(loads_df.loc[idx, 'active_import'] * (alpha)) / 1E6, 5)
f = open('power_diff.csv', 'w')
f.write(f'PDIFF={power_diff}')
f.close()

loads_df.loc[idx, 'active_import'] = loads_df.loc[idx, 'active_import'] * (1 + alpha)


loads_df.time = loads_df.time.astype('str')
data['loads'] = dataframe_to_json(loads_df)

with open('full_data.json', 'w') as outfile:
    json.dump(data, outfile, indent=4)
