# 00 is missing
for HOUR in "10" "11" "12" "13" "14" "15" "16" "17" "18"
do
  ALPHA=0
  VMAX=1.8
  python3 get_voltage_hour.py $HOUR
  source v_lim.csv
  echo $ALPHA
  STATUS="done"
  while [[ $VMAX>$VLIM ]]; do
    if [[ 500 -lt $ALPHA ]]; then
      STATUS="unknown"
      break
    fi
    echo Running hour $HOUR, alpha = $ALPHA
    echo "ALPHA="$ALPHA > alpha.csv
    python3 update_json.py $ALPHA $HOUR

    # Powerflow Solver
    curl --location --request POST 'https://tt-powerflow-api.azurewebsites.net/api/powerflowSolver?format=json&content-type=application/json' \
    --header 'Authorization: Bearer 3GnEKA6BMKh[e/ZW@zDA:uxk8P9SR3r.' -d @full_data.json | tee data/powerflowresults.json

    python3 export_csv_map.py
    source max_v.csv
    ALPHA=$(($ALPHA + 2))
  done
  source power_diff.csv
  echo $HOUR,$(($ALPHA - 2)),$VMAX,$PDIFF,$STATUS >> results.csv
done
